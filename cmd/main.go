package main

import (
	"bytes"
	"errors"
	"fmt"
	"log"
	"os"
	"os/signal"
	"strconv"
	"syscall"

	"github.com/bwmarrin/discordgo"
	"github.com/joho/godotenv"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/alisson-paula/gitlab-discord-go/pkg/command"
)

// Gitlab access token and URL, and Discord bot token
var gitlabAccessToken, gitlabURL, discordBotToken string

// Client struct contains Discord and Gitlab clients
type Client struct {
	Discord *discordgo.Session
	Gitlab  *gitlab.Client
}

var client *Client

func init() {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatalf("error loading .env file: %v", err)
	}

	gitlabAccessToken = os.Getenv("GITLAB_ACCESS_TOKEN")
	gitlabURL = os.Getenv("GITLAB_URL")
	discordBotToken = os.Getenv("DISCORD_BOT_TOKEN")
}

func main() {

	// Initialize client
	client = new(Client)
	var err error

	// Gitlab client initialization
	client.Gitlab, err = gitlab.NewClient(gitlabAccessToken, gitlab.WithBaseURL(gitlabURL))
	if err != nil {
		log.Fatalf("error creating GitLab client: %v", err)
	}

	// Discord client initialization
	client.Discord, err = discordgo.New(discordBotToken)
	if err != nil {
		log.Fatalf("error creating Discord client: %v", err)
	}
	defer client.Discord.Close()

	// Register the onMessageCreate function to the OnMessageCreate event.
	client.Discord.AddHandler(onMessageCreate)

	// Components are part of interactions, so we register InteractionCreate handler
	client.Discord.AddHandler(onInteraction)

	// Open a websocket connection to Discord and begin listening.
	err = client.Discord.Open()
	if err != nil {
		log.Fatalf("error open Discord websocket connection: %v", err)
	}

	// Wait for a interrupt signal to gracefully close the client.
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, syscall.SIGINT, syscall.SIGTERM)
	<-interrupt

}

func onInteraction(s *discordgo.Session, i *discordgo.InteractionCreate) {
	if i.Type != discordgo.InteractionMessageComponent {
		return
	}
	projectID := i.MessageComponentData().CustomID

	state := "opened"
	issues, err := GetIssues(projectID, &gitlab.ListProjectIssuesOptions{
		State: &state,
	})

	if err != nil {
		log.Println(err)
		return
	}

	components := []discordgo.MessageComponent{}

	for _, issue := range issues {
		msg := fmt.Sprintf("%d - %s\n", issue.IID, issue.Title)
		components = append(components, discordgo.ActionsRow{
			Components: []discordgo.MessageComponent{
				discordgo.Button{
					Label:    msg,
					Style:    discordgo.PrimaryButton,
					Disabled: false,
					CustomID: "i-" + strconv.Itoa(issue.IID),
				},
			},
		})

	}

	err = s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseUpdateMessage,
		Data: &discordgo.InteractionResponseData{
			Content:    fmt.Sprintf("Issues no projeto (%d)", len(issues)),
			Components: components,
		},
	})
	if err != nil {
		log.Println(err)
	}
}

// onMessageCreate function is called every time a new message is created on Discord.
func onMessageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	c, err := command.Parse(m.Content)
	if err != nil {
		return
	}

	switch c.Function {
	case command.ProjectsFn:

		l := &gitlab.ListProjectsOptions{}
		if len(c.Args) > 0 {
			search, fsearch := c.Args["search"]
			if fsearch {
				l.Search = &search
			}
		}

		projects, err := GetProjects(l)
		if err != nil {
			log.Println(err)
			return
		}

		components := make([]discordgo.MessageComponent, 0)
		for _, project := range projects {
			components = append(components, discordgo.ActionsRow{
				Components: []discordgo.MessageComponent{
					discordgo.Button{
						Label:    fmt.Sprintf("%d - %s\n", project.ID, project.Name),
						Style:    discordgo.PrimaryButton,
						Disabled: false,
						CustomID: strconv.Itoa(project.ID),
					},
				},
			})
		}

		msg, err := s.ChannelMessageSendComplex(m.ChannelID, &discordgo.MessageSend{
			Content:    fmt.Sprintf("Projetos no GitLab (%d)", len(projects)),
			Components: components,
		})

		log.Printf("%v", err)
		log.Printf("%+v", msg)

		// s.ChannelMessageSendEmbed(m.ChannelID, &discordgo.MessageEmbed{
		// 	Title:       fmt.Sprintf("Projetos no GitLab (%d)", len(projects)),
		// 	Description: "```" + message.String() + "```",
		// 	Color:       0x234e23,
		// })

	case command.IssuesFn:

		if len(c.Args) < 1 {
			return
		}

		pid, fpid := c.Args["pid"]
		if !fpid {
			return
		}

		state := "opened"
		issues, err := GetIssues(pid, &gitlab.ListProjectIssuesOptions{
			State: &state,
		})
		if err != nil {
			log.Println(err)
			return
		}

		var message = bytes.Buffer{}

		for _, issue := range issues {

			msg := fmt.Sprintf("%d - %s\n", issue.IID, issue.Title)

			if message.Len()+len(msg) > 1999 {

				embed := &discordgo.MessageEmbed{
					Title:       fmt.Sprintf("Issues no projeto (%d)", len(issues)),
					Description: "```" + message.String() + "```",
					Color:       0x234e23,
				}

				_, err = s.ChannelMessageSendEmbed(m.ChannelID, embed)
				if err != nil {
					log.Println(err)
				}

				message = bytes.Buffer{}
			}
			message.WriteString(msg)
		}

		embed := &discordgo.MessageEmbed{
			Title:       fmt.Sprintf("Issues no projeto (%d)", len(issues)),
			Description: "```" + message.String() + "```",
			Color:       0x234e23,
		}

		_, err = s.ChannelMessageSendEmbed(m.ChannelID, embed)
		if err != nil {
			log.Println(err)
		}

	case command.IssueFn:

		if len(c.Args) < 1 {
			return
		}

		pid, fpid := c.Args["pid"]
		if !fpid {
			return
		}

		iid, fiid := c.Args["iid"]
		if !fiid {
			return
		}
		issueID, _ := strconv.Atoi(iid)

		issue, _, err := client.Gitlab.Issues.GetIssue(pid, issueID)
		if err != nil {
			log.Println("Erro ao listar projetos do GitLab")
			return
		}

		_, err = s.ChannelMessageSendEmbed(m.ChannelID, &discordgo.MessageEmbed{
			Title:       fmt.Sprintf("Issue %s", issue.Title),
			Description: "```" + issue.Description + "```",
			Color:       0x234e23,
		})
		if err != nil {
			log.Println(err)
		}

	default:
		log.Println("invalid command")
	}
}

// GetProjects get projects from GitLab API, independent of pagination.
func GetProjects(l *gitlab.ListProjectsOptions) ([]*gitlab.Project, error) {
	projects, response, err := client.Gitlab.Projects.ListProjects(l)
	if err != nil {
		return nil, errors.New("Erro ao listar projetos do GitLab")
	}

	if response.NextPage > 0 && response.NextPage != response.CurrentPage {
		l.ListOptions.Page = response.NextPage
		next, err := GetProjects(l)
		if err != nil {
			return nil, err
		}
		projects = append(projects, next...)
	}
	return projects, nil
}

// GetIssues get project issues from GitLab API, independent of pagination.
func GetIssues(pid string, l *gitlab.ListProjectIssuesOptions) ([]*gitlab.Issue, error) {
	issues, response, err := client.Gitlab.Issues.ListProjectIssues(pid, l)
	if err != nil {
		return nil, errors.New("Erro ao listar projetos do GitLab")
	}

	if response.NextPage > 0 && response.NextPage != response.CurrentPage {
		l.ListOptions.Page = response.NextPage
		next, err := GetIssues(pid, l)
		if err != nil {
			return nil, err
		}
		issues = append(issues, next...)
	}
	return issues, nil
}
