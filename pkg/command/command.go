package command

import (
	"fmt"
	"strings"
)

// Command struct represents a parsed command
type Command struct {
	Function string
	Args     map[string]string
}

// Command consts
const (
	gitlabPrefix = "!gitlab "
	ProjectsFn   = "projects"
	IssuesFn     = "issues"
	IssueFn      = "issue"
)

// Parse parses a command and returns a Command struct
func Parse(command string) (*Command, error) {

	// Check if command has the expected prefix
	if !strings.HasPrefix(command, gitlabPrefix) {
		return nil, fmt.Errorf("invalid command prefix")
	}
	command = strings.TrimPrefix(command, gitlabPrefix)

	// Initialize a new Command struct
	c := &Command{
		Args: make(map[string]string),
	}

	// Keep track of the most recently seen key
	var tmpKey string

	// Loop through each word in the command
	for i, v := range strings.Split(command, " ") {
		switch {
		// The first word is the function name
		case i == 0:
			c.Function = v
		// The word starts with "--" and is a key
		case strings.HasPrefix(v, "--"):
			tmpKey = strings.TrimPrefix(v, "--")
			c.Args[tmpKey] = ""
		// The word is a value for the most recently seen key
		case len(tmpKey) > 0:
			c.Args[tmpKey] = v
			tmpKey = ""
		}
	}
	return c, nil
}
