# Gitlab-Discord bot

This is a bot that integrates GitLab with Discord. It receives GitLab commands and sends their results to a Discord channel.

## Prerequisites

To run the project, you need to have GitLab and Discord accounts, and a Bot token for Discord. Additionally, the following Go modules are required:

- [github.com/bwmarrin/discordgo](https://github.com/bwmarrin/discordgo)  
- [github.com/xanzy/go-gitlab](https://github.com/xanzy/go-gitlab)  

## Installing

To install the bot, follow these steps:

Clone the project to your local machine:

```sh
git clone https://gitlab.com/alisson-paula/gitlab-discord-go.git
```

Change to the project directory:

```sh
cd gitlab-discord-go
```

Replace the empty GITLAB_ACCESS_TOKEN, GITLAB_URL, and DISCORD_BOT_TOKEN constants in the .env file with your GitLab token, GitLab URL, and Discord bot token, respectively.

Build the bot using the go build command:

```sh
go build -o gitlab-discord-go cmd/main.go
```

Run the bot using the generated binary:

```sh
./gitlab-discord-go
```

## Usage

To use the bot, send GitLab commands to your Discord channel in the format **`'!gitlab <command> --<arg>=<value>'`**. For example, to list all the projects in your GitLab account, send the following command to your Discord channel:

```sh
!gitlab projects
```

You can also filter the results of a command using arguments. For example, to search for projects containing the word "example", send the following command to your Discord channel:

```sh
!gitlab projects --search=example
```

The following GitLab commands are currently supported:

**`'projects'`**: List all projects in your GitLab account. Supports the **`'--search'`** argument to filter results by project name.  
**`'issues'`**: List all issues in a project. Requires the **`'--pid'`** argument to specify the project ID. Supports the **`'--search'`** argument to filter results by issue title.  
**`'issue'`**: Get the details of a single issue. Requires the **`'--pid'`** and **`'--iid'`** arguments to specify the project ID and issue ID, respectively.  

## Contributing

Contributions to the project are welcome. If you would like to contribute, please fork the project and submit a pull request.
